package com.netcracker.suits.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Дмитрий on 18.04.2016.
 */
@Controller
public class HrController {

    @RequestMapping(value="/hr_history", method = RequestMethod.GET)
    public String getHistory(){

        return "hr/hr_history";
    }

    @RequestMapping(value="/hr_time", method = RequestMethod.GET)
    public String getTime(){

        return "hr/hr_time";
    }

    @RequestMapping(value="/hr", method = RequestMethod.GET)
    public String getHomeHr(){

        return "hr/hr";
    }

    @RequestMapping(value="/hr_profiles", method = RequestMethod.GET)
    public String getProfiles(){

        return "hr/hr_profiles";
    }
}
