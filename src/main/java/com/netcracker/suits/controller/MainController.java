package com.netcracker.suits.controller;

import com.netcracker.suits.infrastructure.TransactionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.sql.SQLException;


/**
 * Created by Olesya on 12.04.2016.
 */
@Controller
public class MainController {

    @Autowired
    TransactionManager transactionManager;

    @RequestMapping(value = {"/", "/index"}, method = RequestMethod.GET)
    public String printWelcome() throws SQLException {
        return "index";
    }

    @RequestMapping(value = "/company", method = RequestMethod.GET)
    public String companyPage() {
        return "company";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String registrationPage() {
        return "registration";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView login(
            @RequestParam(value = "error", required = false) String error,
            @RequestParam(value = "logout", required = false) String logout) {

        ModelAndView model = new ModelAndView();
        if (error != null) {
            model.addObject("error", "Invalid username and password!");
        }

        if (logout != null) {
            model.addObject("msg", "You've been logged out successfully.");
        }
        model.setViewName("login");

        return model;

    }
}
