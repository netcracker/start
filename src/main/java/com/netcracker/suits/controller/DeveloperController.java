package com.netcracker.suits.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by beerman on 18.04.2016.
 */
@Controller
public class DeveloperController {

    @RequestMapping(value = "/dev", method = RequestMethod.GET)
    public String developerHome() {
        return "/dev/dev";
    }

    @RequestMapping(value = "/profiles", method = RequestMethod.GET)
    public String developerProfiles() {
        return "/dev/dev_profiles";
    }

    @RequestMapping(value = "/time", method = RequestMethod.GET)
    public String developerTime() {
        return "/dev/dev_time";
    }
}
