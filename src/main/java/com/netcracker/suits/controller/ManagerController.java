package com.netcracker.suits.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author Yuliia Nemyrovets
 */
@Controller
public class ManagerController {
    @RequestMapping(value = "/manager_page", method = RequestMethod.GET)
    public String managerPage() {
        return "manager/manager_page";
    }

    @RequestMapping(value = "/manager_settings", method = RequestMethod.GET)
    public String managerSettingPage() {
        return "manager/manager_settings";
    }

    @RequestMapping(value = "/manager_questions", method = RequestMethod.GET)
    public String managerListOfQuestions() {
        return "manager/manager_questions";
    }

    @RequestMapping(value = "/manager_interviewers", method = RequestMethod.GET)
    public String managerInterviewers() {
        return "manager/manager_interviewers";
    }

    @RequestMapping(value = "/manager_loading", method = RequestMethod.GET)
    public String managerLoad() {
        return "manager/manager_loading";
    }


    @RequestMapping(value = "/manager_timetable", method = RequestMethod.GET)
    public String managerSchedule() {
        return "manager/manager_timetable";
    }

    @RequestMapping(value = "/manager_form", method = RequestMethod.GET)
    public String managerStudentForm() {
        return "manager/manager_form";
    }

    @RequestMapping(value = "/manager_interviewers2", method = RequestMethod.GET)
    public String managerInterviewers2() {
        return "manager/manager_interviewers2";
    }


}
