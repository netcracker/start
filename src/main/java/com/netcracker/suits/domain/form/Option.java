package com.netcracker.suits.domain.form;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Misha Hliba
 */

public class Option {

    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("checked")
    @Expose
    private boolean checked;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Option withLabel(String label) {
        this.label = label;
        return this;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public Option withChecked(boolean checked) {
        this.checked = checked;
        return this;
    }

}