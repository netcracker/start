
package com.netcracker.suits.domain.form;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

/**
 * @author Misha Hliba
 */

public class FieldElement {

    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("field_type")
    @Expose
    private String fieldType;
    @SerializedName("required")
    @Expose
    private boolean required;
    @SerializedName("field_options")
    @Expose
    private FieldOptions fieldOptions;
    @SerializedName("cid")
    @Expose
    private String cid;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public FieldElement withLabel(String label) {
        this.label = label;
        return this;
    }


    public String getFieldType() {
        return fieldType;
    }

    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }

    public FieldElement withFieldType(String fieldType) {
        this.fieldType = fieldType;
        return this;
    }


    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public FieldElement withRequired(boolean required) {
        this.required = required;
        return this;
    }

    public FieldOptions getFieldOptions() {
        return fieldOptions;
    }

    public void setFieldOptions(FieldOptions fieldOptions) {
        this.fieldOptions = fieldOptions;
    }

    public FieldElement withFieldOptions(FieldOptions fieldOptions) {
        this.fieldOptions = fieldOptions;
        return this;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public FieldElement withCid(String cid) {
        this.cid = cid;
        return this;
    }

}