
package com.netcracker.suits.domain.form;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Misha Hliba
 */

public class FieldOptions {

    @SerializedName("options")
    @Expose
    private List<Option> options = new ArrayList<Option>();

    public List<Option> getOptions() {
        return options;
    }

    public void setOptions(List<Option> options) {
        this.options = options;
    }

    public FieldOptions withOptions(List<Option> options) {
        this.options = options;
        return this;
    }

}