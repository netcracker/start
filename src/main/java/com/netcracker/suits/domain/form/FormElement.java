
package com.netcracker.suits.domain.form;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
    * @author Misha Hliba
 */
public class FormElement {

    @SerializedName("fields")
    @Expose
    private List<FieldElement> fields = new ArrayList<FieldElement>();

    public List<FieldElement> getFields() {
        return fields;
    }

    public void setFields(List<FieldElement> fields) {
        this.fields = fields;
    }

    public FormElement withFields(List<FieldElement> fields) {
        this.fields = fields;
        return this;
    }

}