package com.netcracker.suits.domain.form;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Misha Hliba
 */

public class Form {

    @SerializedName("fields")
    @Expose
    private List<FieldElement> element;

    public void setElement(List<FieldElement> element){
        this.element = element;
    }

    public List<FieldElement> getElement(){
        return element;
    }
}
