package com.netcracker.suits.domain;

/**
 * Created by Olesya on 17.04.2016.
 */
public class AccountRole {
    private Integer id;
    private Account account;
    private Role role;

    public AccountRole() {
    }

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    public Account getAccount() {
        return account;
    }
    public void setAccount(Account account) {
        this.account = account;
    }

    public Role getRole() {
        return role;
    }
    public void setRole(Role role) {
        this.role = role;
    }
}
