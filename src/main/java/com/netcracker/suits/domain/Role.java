package com.netcracker.suits.domain;

/**
 * Created by Olesya on 17.04.2016.
 */
public class Role {
    private Integer id;
    private String name;

    public Role() {
    }

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
}
