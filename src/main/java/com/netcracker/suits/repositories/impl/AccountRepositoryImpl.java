package com.netcracker.suits.repositories.impl;

import com.netcracker.suits.domain.Account;
import com.netcracker.suits.infrastructure.impl.SimpleCrudRepository;
import com.netcracker.suits.repositories.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by Olesya on 17.04.2016.
 */

@Repository
public class AccountRepositoryImpl extends SimpleCrudRepository<Account, Integer> implements AccountRepository {
    @Autowired
    DataSource dataSource;

    @Override
    public Account findByEmail(String email) {
        Account account = null;
        try (Connection connection = dataSource.getConnection()) {
            try (Statement statement = connection.createStatement()) {
                String sql = "SELECT * FROM account WHERE email='" + email + "'";
                try (ResultSet resultSet = statement.executeQuery(sql)) {
                    if (!resultSet.next()) {
                        return null;
                    } else {
                        Account currentAccount = new Account();

                        currentAccount.setId(resultSet.getInt("id"));
                        currentAccount.setEmail(resultSet.getString("email"));
                        currentAccount.setPassword(resultSet.getString("password"));
                        currentAccount.setFirstName(resultSet.getString("first_name"));
                        currentAccount.setSecondName(resultSet.getString("second_name"));
                        currentAccount.setLastName(resultSet.getString("last_name"));
                        currentAccount.setFirstName(resultSet.getString("first_name"));
                        currentAccount.setEnabled(resultSet.getBoolean("enabled"));

                        account = currentAccount;
                    }
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return account;
    }
}
