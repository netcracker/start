package com.netcracker.suits.repositories.impl;

import com.netcracker.suits.domain.AccountRole;
import com.netcracker.suits.infrastructure.impl.SimpleCrudRepository;
import com.netcracker.suits.repositories.AccountRoleRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Olesya on 18.04.2016.
 */
@Repository
public class AccountRoleRepositoryImpl extends SimpleCrudRepository<AccountRole, Integer> implements AccountRoleRepository {
}
