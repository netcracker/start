package com.netcracker.suits.repositories.impl;

import com.netcracker.suits.domain.Role;
import com.netcracker.suits.infrastructure.impl.SimpleCrudRepository;
import com.netcracker.suits.repositories.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Olesya on 18.04.2016.
 */
@Repository
public class RoleRepositoryImpl extends SimpleCrudRepository<Role, Integer> implements RoleRepository {
    @Autowired
    DataSource dataSource;

    @Override
    public List<Role> findForAccountId(int accountId) {
        List<Role> roles = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            try (Statement statement = connection.createStatement()) {
                String sql = "SELECT * FROM role WHERE id IN(" +
                        "SELECT role_id FROM account_role WHERE account_id=" + accountId + ")";
                try (ResultSet resultSet = statement.executeQuery(sql)) {
                    while (resultSet.next()) {
                        Role role = new Role();

                        role.setId(resultSet.getInt("id"));
                        role.setName(resultSet.getString("name"));

                        roles.add(role);
                    }
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return roles;
    }
}
