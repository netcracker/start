package com.netcracker.suits.repositories;

import com.netcracker.suits.domain.AccountRole;
import com.netcracker.suits.infrastructure.CrudRepository;

/**
 * Created by Olesya on 18.04.2016.
 */
public interface AccountRoleRepository extends CrudRepository<AccountRole, Integer> {
}
