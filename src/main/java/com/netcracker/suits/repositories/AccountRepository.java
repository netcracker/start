package com.netcracker.suits.repositories;

import com.netcracker.suits.domain.Account;
import com.netcracker.suits.infrastructure.CrudRepository;

/**
 * Created by Olesya on 17.04.2016.
 */
public interface AccountRepository extends CrudRepository<Account, Integer> {

    Account findByEmail(String email);
}
