package com.netcracker.suits.repositories;

import com.netcracker.suits.domain.Role;
import com.netcracker.suits.infrastructure.CrudRepository;

import java.util.List;

/**
 * Created by Olesya on 18.04.2016.
 */
public interface RoleRepository extends CrudRepository<Role, Integer> {
    List<Role> findForAccountId(int accountId);
}
