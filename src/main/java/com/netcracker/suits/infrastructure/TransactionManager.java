package com.netcracker.suits.infrastructure;

import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.SQLException;

/**
 * Created by Olesya on 4/20/2016.
 */
public interface TransactionManager {
    void begin() throws SQLException;

    void commit() throws SQLException;

    void rollback() throws SQLException;

    JdbcTemplate getJdbcTemplate();
}
