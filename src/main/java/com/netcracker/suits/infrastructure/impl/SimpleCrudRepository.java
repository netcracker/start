package com.netcracker.suits.infrastructure.impl;

import com.netcracker.suits.infrastructure.CrudRepository;

import java.io.Serializable;

/**
 * Created by Olesya on 17.04.2016.
 */
// TODO: Implement all methods
public class SimpleCrudRepository<T, ID extends Serializable> implements CrudRepository<T, ID> {

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void delete(ID id) {

    }

    @Override
    public void delete(Iterable<? extends T> entities) {

    }

    @Override
    public void delete(T entity) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public boolean exists(ID id) {
        return false;
    }

    @Override
    public Iterable<T> findAll() {
        return null;
    }

    @Override
    public T findOne(ID id) {
        return null;
    }

    @Override
    public Iterable<T> save(Iterable<? extends T> entities) {
        return null;
    }

    @Override
    public T save(T entity) {
        return null;
    }
}
