package com.netcracker.suits.infrastructure.impl;

import com.netcracker.suits.infrastructure.TransactionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;


/**
 * Created by Olesya on 4/20/2016.
 */
@Component
@Scope(scopeName = "request", proxyMode = ScopedProxyMode.INTERFACES)
public class TransactionManagerImpl implements TransactionManager {
    @Autowired
    private DataSource dataSource;

    private Connection connection;

    private JdbcTemplate jdbcTemplate;

    private int transactionCounter = 0;

    @PostConstruct
    private void init() {
        TransactionSynchronizationManager.initSynchronization();
    }

    @PreDestroy
    private void destroy() {
        TransactionSynchronizationManager.clear();
    }

    @Override
    public void begin() throws SQLException {
        if (connection == null) {
            connection = DataSourceUtils.getConnection(dataSource);
        }
        if (transactionCounter == 0) {
            connection.setAutoCommit(false);
        }
        transactionCounter++;
    }

    @Override
    public void commit() throws SQLException {
        requiredTransaction();
        if (transactionCounter == 1) {
            connection.commit();
            connection.setAutoCommit(true);
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        transactionCounter--;
    }

    @Override
    public void rollback() throws SQLException {
        requiredTransaction();
        connection.rollback();
        connection.setAutoCommit(true);
        DataSourceUtils.releaseConnection(connection, dataSource);
        transactionCounter = 0;
    }

    @Override
    public JdbcTemplate getJdbcTemplate() {
        if (jdbcTemplate == null) {
            jdbcTemplate = new JdbcTemplate(dataSource);
        }
        return jdbcTemplate;
    }

    private void requiredTransaction() throws SQLException {
        if (connection == null || transactionCounter == 0) {
            throw new SQLException();
        }
    }
}
