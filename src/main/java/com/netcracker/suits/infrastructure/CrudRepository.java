package com.netcracker.suits.infrastructure;

import java.io.Serializable;

/**
 * Created by Olesya on 17.04.2016.
 */
public interface CrudRepository<T, ID extends Serializable> {
    long count();

    void delete(ID id);

    void delete(Iterable<? extends T> entities);

    void delete(T entity);

    void deleteAll();

    boolean exists(ID id);

    Iterable<T> findAll();

    T findOne(ID id);

    Iterable<T> save(Iterable<? extends T> entities);

    T save(T entity);
}
