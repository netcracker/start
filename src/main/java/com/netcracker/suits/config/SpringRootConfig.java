package com.netcracker.suits.config;

import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.ViewResolver;
import org.thymeleaf.extras.springsecurity4.dialect.SpringSecurityDialect;
import org.thymeleaf.spring4.SpringTemplateEngine;
import org.thymeleaf.spring4.view.ThymeleafViewResolver;
import org.thymeleaf.templateresolver.ServletContextTemplateResolver;
import org.thymeleaf.templateresolver.TemplateResolver;

import javax.annotation.Resource;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * Created by Olesya on 12.04.2016.
 */
@Configuration
@PropertySource("classpath:app.properties")
@ComponentScan({"com.netcracker.suits", "com.netcracker.suits.controller"})
@Import({SecurityConfig.class, ThymeleafConfig.class})
@EnableTransactionManagement
public class SpringRootConfig {

    private static final String PROP_DS_JNDI_NAME = "datasource.jndiName";

    @Resource
    private Environment env;

    @Bean
    DataSource dataSource() {
        DataSource ds = null;
        try {
            ds = (DataSource) new InitialContext().lookup(env.getRequiredProperty(PROP_DS_JNDI_NAME));
        } catch (NamingException e) {
            e.printStackTrace();
        }

        return ds;
    }
}
