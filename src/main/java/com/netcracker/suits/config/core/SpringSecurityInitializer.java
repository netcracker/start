package com.netcracker.suits.config.core;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Created by Olesya on 12.04.2016.
 */
public class SpringSecurityInitializer extends AbstractSecurityWebApplicationInitializer {

}
