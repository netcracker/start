package com.netcracker.suits.authentication;

import com.netcracker.suits.domain.Account;
import com.netcracker.suits.domain.Role;
import com.netcracker.suits.repositories.AccountRepository;
import com.netcracker.suits.repositories.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Olesya on 18.04.2016.
 */
@Service("userDetailsService")
public class MyUserDetailsService implements UserDetailsService {
    @Autowired
    AccountRepository accountRepository;

    @Autowired
    RoleRepository roleRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Account account = accountRepository.findByEmail(email);
        if (account == null) {
            throw new UsernameNotFoundException("There is no user with such email");
        }

        return buildUserDetails(account);
    }

    private UserDetails buildUserDetails(Account account) {
        List<GrantedAuthority> authorities = getGrantedAuthoritiesForAccount(account);
        return new MyUserDetails(account, authorities);
    }

    private List<GrantedAuthority> getGrantedAuthoritiesForAccount(Account account) {
        List<Role> roles = roleRepository.findForAccountId(account.getId());
        List<GrantedAuthority> authorities = new ArrayList<>();


        for (Role role : roles) {
            authorities.add(new SimpleGrantedAuthority(role.getName()));
        }
        return authorities;
    }

}
