$(function() {
    $('button').on('hidden.bs.tooltip', function () {

    });



    var app = {

        initialize : function () {
            this.modules();
            this.setUpListeners();
        },

        modules: function () {

        },

        setUpListeners: function () {
            $('form').on('submit', app.submitForm);
            $('form').on('keydown', 'input', app.removeError);
        },

        submitForm: function (e) {
            e.preventDefault();
            var form = $(this);

            if(app.validateForm(form) == false) return false;
            console.log('form not valid');
        },

        validateForm: function(form){
            console.log('valideta');
            var inputs = form.find("input");
            valid = true;


            /* inputs.tooltip('destroy');*/


            $.each(inputs,function(index,val){
                var input = $(val),
                    val = input.val();
                formGoup = input.parents('.form-group:not(#checkbox_group)'),
                    lable = formGoup.find('lable').text(),
                    textError = 'Введите ' + lable;

                if(val.length === 0){
                    formGoup.addClass('has-error').removeClass('has-success');
                    /*input.tooltip({
                     trigger: 'manual',
                     placement: 'right',
                     title: textError
                     }).tooltip('show');*/
                    valid = false;
                }else{
                    formGoup.addClass('has-success').removeClass('has-error');
                }

            });



            return valid;
        },
        removeError: function(){
            $(this).parents('.form-group').removeClass('has-error');
            /*        if(validateEmail($(this).val()) == true){
             formGoup.addClass('has-success').removeClass('has-error');
             }*/
        }


        /*    validateEmail: function(AInputText) {
         var VRegExp = new RegExp(/(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/);
         var VResult = VRegExp.exec(AInputText);
         return VResult;
         },*/

    }

    app.initialize();


}());

