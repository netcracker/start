

/**
 * @author: Yashchuk A.F.
 * descriptions: function initialize form-builder and programing button save (#savebtn)
 */
$(document).ready(function () {
    var initfombuilder = [];


    var objStringifiedAndEncoded = location.search.substr(1);
    var formbuilder;
    /* if no parameters init clean formbuilder*/
    if (objStringifiedAndEncoded.length === 0) {
        formbuilder = new Formbuilder({ selector: '#formbuilder' });
    }
    /* init form builder with parameters */
    else {
        objStringified = decodeURIComponent(objStringifiedAndEncoded);
        obj = JSON.parse(objStringified);
        console.log(obj);
        console.log(initfombuilder);
        jQuery.each(obj, function () {
            for (i = 0; i < this.length; i++) {
                initfombuilder.push(this[i]);
            }
        });
        formbuilder = new Formbuilder({
            selector: '#formbuilder',
            bootstrapData: initfombuilder
        });
    }


    var tmpView;

    formbuilder.on('save', function (payload) {
        /*console.log(payload);*/
        tmpView = payload;

        $.ajax({
            type:'GET',
            url: "saveForm",
            data:{
                form:payload},
            success:function(data){
                alert(data);
            }
        })
    })


    $('#savebtn').click(function () {
        console.log(tmpView);



    });
});

/**
 * @author: Yashchuk A.F.
 * descriptions: function represent form-builder, hidden extra elements and review button paragaph
 */
$( document ).ready(function() {
    var elements = $('#main .section > :gt(5)');
    /* hidden extra elements */
    elements.css("visibility", "hidden");
    elements.click(function(){
        event.stopPropagation();
    });
    $('#main .edit-response-field > :gt(2)').css("visibility", "hidden");
    /*change name for button paragraph*/
    $('#main .section > :eq(1)').html('<span class="glyphicon glyphicon-font"></span> 	Text area');
    $('#main .section > :eq(0)').html('<span class="glyphicon glyphicon-minus"></span> 	Input field');
    $('#main .section > :eq(4)').css("visibility", "hidden");
    /* hidden extra elements */
    $('#main .section > :eq(4)').click(function(){
        event.stopPropagation();
    });
});

