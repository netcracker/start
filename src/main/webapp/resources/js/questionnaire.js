
/**
 * @author: Yashchuk A.F.
 * descriptions: function view test Json parsing for dynamic questionnaire
 */
$(document).ready(function(){
    /* lisener on button #gen*/
    $('#gen').click(function(){
        var json = $('#jsonInput').val();
        $('#jsonInput').val("");
        $('#main form').empty();
        createView(json);
    });
    /* default questionnaire*/
    var jsonObj = '{"fields":[{"label":"Введите имя","field_type":"text","required":true,"field_options":{"size":"small","description":"Ваше имя"},"cid":"c38"},{"label":"Введите фамилию","field_type":"text","required":true,"field_options":{"size":"small","description":"Ваша фамилия"},"cid":"c42"},{"label":"Что для вас интересно?","field_type":"checkboxes","required":true,"field_options":{"options":[{"label":"Бизнес анализ","checked":false},{"label":"Разработка ПО","checked":false},{"label":"Тестирование ПО","checked":false},{"label":"Промышленые сети","checked":false}]},"cid":"c46"},{"label":"Оцените свои знания Java","field_type":"radio","required":true,"field_options":{"options":[{"label":"не заню","checked":false},{"label":"знаю синтаксис","checked":false},{"label":"знаю синтаксис и библиотеки","checked":false},{"label":"Хорошо знаю, имею опыт работы","checked":false}]},"cid":"c50"},{"label":"Выберите вуз","field_type":"dropdown","required":true,"field_options":{"options":[{"label":"КПИ","checked":false},{"label":"НАУ","checked":false},{"label":"КНЕУ","checked":false},{"label":"Шева","checked":false}],"include_blank_option":false},"cid":"c54"},{"label":"Раскажите смешную историю о себе","field_type":"paragraph","required":true,"field_options":{"size":"small"},"cid":"c58"}]}';
    createView(jsonObj);
});

/**
 * @author: Yashchuk A.F.
 * descriptions: function parse json data and view dynamic questionnaire
 * input param:
 * jsonModel - json model questionnaire
 */
function createView(jsonModel){
    obj = JSON.parse(jsonModel);

    $.each( obj.fields, function(key, value) {
        var lable;
        var field_type;
        var required;
        var value_iteams = [];
        var name = "";
        var helpBlock;


        // parse input json data
        $.each( value, function(key1, value1) {
            if(key1 === "label"){
                lable = value1;
                console.log(lable);
            }if(key1 == "field_type"){
                field_type = value1;
                console.log(field_type);
            }if(key1 == "required"){
                required = value1;
                console.log(required);
            }if(key1 == "field_options"){
                if(value1.options != null){
                    i = 0;
                    $.each( value1.options, function(key2, value2) {
                        console.log(value2);
                        value_iteams[i] = value2.label;
                        i++;
                    });
                }
                helpBlock = value1.description;
                console.log(helpBlock);
            }
        });

        // create html view according field_type
        if(field_type == "text"){
            textFieldView(lable,name,helpBlock,required);
        }if(field_type == "checkboxes"){
            checkboxesView(lable,value_iteams,name,required);
        }if(field_type == "radio"){
            radioView(lable,value_iteams,lable,required);
        }if(field_type == "dropdown"){
            dropdownView(lable,value_iteams,lable,required);
        }if(field_type == "paragraph"){
            textAreaView(lable, name, required);
        }
    });

    // add button submit on the end questionnaire
    $('#main form').append('<button type="submit" class="btn btn-default button1">Save</button>');
}


/**
 * @author: Yashchuk A.F.
 * descriptions: function create input text field
 * input param:
 * label - title
 * name - input`s name
 * helpBlock - help-block (html)
 * required - true if input compulsory
 *
 * JSON view:
 *        {
 *                label: "label",
 *                field_type: "text",
 *                "required":true,
 *                "field_options":{
 *                                    "description": helpBlock
 *                                }
 *                "description": helpBlock
 *                name: name
 *        }
 */
function textFieldView(label, name, helpBlock,required ){
    if(required == true){
        $('#main form').append('<div class="form-group"><div><label class="control-label">'+label+':</label><input name="'+name+'" class="form-control" type="text" required /><span class="help-block">'+helpBlock+'</span></div></div>');
    }else{
        $('#main form').append('<div class="form-group"><div><label class="control-label">'+label+':</label><input name="'+name+'" class="form-control" type="text"/><span class="help-block">'+helpBlock+'</span></div></div>');
    }
}
/**
 * @author: Yashchuk A.F.
 * descriptions: function create checkboxes view
 * input param:
 * label - title
 * options - checkboxes values
 * name - input`s name
 * required - true if input compulsory
 *
 *    JSON view:
 *        {
 *                "label":"label",
 *                "field_type":"checkboxes",
 *                "required":true,
 *                "field_options":
 *                                {
 *                                   "options":[
 *                                                {"label":"1","checked":false},
 *                                                {"label":"2","checked":false}
 *                                              ]
 *                                    "description": helpBlock
 *                                }
 *                "name": "name"
 *        }
 */
function checkboxesView(label, options,name, required ){
    $('#main form').append('<div class="form-group"></div>');
    $('form > div').last().append('<label class="control-label">'+label+':</label>');
    $('form > div').last().append('</br>');
    for(i=0; i<options.length; i++){
        if(required == true){
            $('form > div').last().append('<label class="checkbox-inline"><input type="checkbox" id="inlineCheckbox'+i+'" required name="'+name+i+'" value="option'+i+'">'+options[i]+'</label>');
        }else{
            $('form > div').last().append('<label class="checkbox-inline"><input type="checkbox" id="inlineCheckbox'+i+'" name="'+name+i+'" value="option'+i+'">'+options[i]+'</label>');
        }
    }
}
/**
 * @author: Yashchuk A.F.
 * descriptions: function create radio view
 * input param:
 * label - title
 * options - radio values
 * name - input`s name
 * required - true if input compulsory
 *
 * JSON view:
 *            {
 *                    "label":"label",
 *                    "field_type":"radio",
 *                    "required":true,
 *                    "field_options":
 *                                    {
 *                                    "options":[
 *                                                {"label":"1","checked":false},
 *                                                {"label":"2","checked":false}
 *                                    ]
 *                                    "description": helpBlock
 *                                   }
 *                    "name": "name"
 *           }
 */
function radioView(label, options, name, required){
    $('#main form').append('<div class="form-group"></div>');
    $('form > div').last().append('<label class="control-label">'+label+':</label>');
    $('form > div').last().append('</br>');
    for(i=0; i<options.length; i++){
        if(required == true){
            $('form > div').last().append('<label class="radio-inline"><input type="radio" required name="'+name+'" id="inlineRadio'+i+'" value="option'+i+'">'+options[i]+'</label>');
        }else{
            $('form > div').last().append('<label class="radio-inline"><input type="radio" name="'+name+'" id="inlineRadio'+i+'" value="option'+i+'">'+options[i]+'</label>');
        }
    }
}

/**
 * @author: Yashchuk A.F.
 * descriptions: function create text area
 * input param:
 * label - title
 * name - input`s name
 * required - true if input compulsory
 *
 * JSON view:
 *           {
 *                label: "label",
 *                field_type: "textarea",
 *                "required":true,
 *                "field_options":{
 *                                   "description": helpBlock
 *                                }
 *                name: name
 *            }
 */
function textAreaView(label,name, required ){
    $('#main form').append('<div class="form-group"></div>');
    $('form > div').last().append('<label class="control-label">'+label+':</label>');
    if(required == true){
        $('form > div').last().append('<textarea class="form-control" required rows="3"></textarea>');
    }else{
        $('form > div').last().append('<textarea class="form-control" rows="3"></textarea>');
    }

}
/**
 * @author: Yashchuk A.F.
 * descriptions: function create dropdown view
 * input param:
 * label - title
 * options - dropdown values
 * name - input`s name
 * required - true if input compulsory
 *
 * JSON view:
 *            {
 *                    "label":"Untitled",
 *                    "field_type":"dropdown",
 *                    "required":true,
 *                    "field_options":{
 *                                    "options":[
 *                                                {"label":"1","checked":false},
 *                                                {"label":"2","checked":false}],
 *                                              ]
 *                                    "description": helpBlock
 *                                   },
 *                    name:"name"
 *            }
 */
function dropdownView(label,options, name, required){
    $('#main form').append('<div class="form-group"></div>');
    $('form > div').last().append('<label class="control-label">'+label+':</label>');
    $('form > div').last().append('</br>');
    if(required == true){
        $('form > div').last().append('<select name="'+name+'" required class="form-control"></select>');
    }else{
        $('form > div').last().append('<select name="'+name+'" class="form-control"></select>');
    }
    for(i=0; i<options.length; i++){
        $('form > div select').last().append('<option>'+options[i]+'</option>');
    }
}
