/**
 * @author: Yashchuk A.F.
 * descriptions: function delete .titleee_active class from all divs .titleee in #main_div3
 */
function deleteHovered(){
    var titleees = jQuery(".titleee");
    titleees.removeClass("titleee_active");
}

/**
 * @author: Yashchuk A.F.
 * descriptions: function set visible block in panel (#main_div_2)
 */
function setDivVisible(val){
    var blocks = $('.panel-body >');
   /* hidden all*/
    blocks.css({"display": "none", "visibility": "hidden"});
    /* set visible needed block */
    $('.panel-body >:eq(' + val +')').css({"display":"block", "visibility":"visible"});
}
/* anchor animate  */


/**
 * @author: Yashchuk A.F.
 * descriptions: function animate anchor slowly
 */
jQuery(document).ready(function($){
    $(this).on('click', 'a[href^=#]', function () {
        $('html, body').animate({ scrollTop:  $('a[id="'+this.hash.slice(1)+'"]').offset().top }, 1000 );
        return false;
    });
});

/**
 * @author: Yashchuk A.F.
 * descriptions: function animation opacity if mouse focused on icon
 */
$(document).ready(function(){
    $('#main_div_2 span').each(function(){
        $(this).animate({opacity:'0.7'},1);
        $(this).mouseover(function(){
            $(this).stop().animate({opacity:'1.0'},600);
        });       $(this).mouseout(function(){
            $(this).stop().animate({opacity:'0.7'},300);
        });
    });
});

/**
 * @author: Yashchuk A.F.
 * descriptions: functions add listeners for divs where id = "#icon"
 */
$(document).ready(function(){
    jQuery("#icon1").click(function(){
        deleteHovered();
        $('.panel-heading h3').html("Кого мы ищем?");
        $(".titleee:eq(0)").addClass("titleee_active");
        setDivVisible(0);
    });
});
$(document).ready(function(){
    jQuery("#icon2").click(function(){
        deleteHovered();
        $('.panel-heading h3').html("О курсах:");
        $(".titleee:eq(1)").addClass("titleee_active");
        setDivVisible(1);
    });
});
$(document).ready(function(){
    jQuery("#icon3").click(function(){
        deleteHovered();
        $('.panel-heading h3').html("Чтобы попасть на курсы нужно:");
        $(".titleee:eq(2)").addClass("titleee_active");
        setDivVisible(2);
    });
});